/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libmat.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 14:28:36 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/14 16:05:18 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBMAT_H
# define LIBMAT_H

# define AXIS_X 0
# define AXIS_Y 1
# define AXIS_Z 2
# define PI 3.141592
# define IDENTITY 0x7FFFFFFF

# include <math.h>

typedef struct	s_mat4
{
	float	    m[16];
}				t_mat4;

typedef struct	s_vec4
{
	float	    v[4];
}				t_vec4;

typedef struct	s_vec3
{
	float	    v[3];
}				t_vec3;

t_vec3			vec3(float x, float y, float z);
t_vec4			vec4(float x, float y, float z, float w);

t_vec3      	vec3_add(t_vec3 vec1, t_vec3 vec2);
t_vec3      	vec3_sub(t_vec3 vec1, t_vec3 vec2);
t_vec3      	vec3_cross(t_vec3 vec1, t_vec3 vec2);
float			vec3_dot(t_vec3 vec1, t_vec3 vec2);
t_vec3			vec3_fmul(t_vec3 v, float f);

float   		vec3_magnitude(t_vec3 vec);
t_vec3      	vec3_normalize(t_vec3 vec3);
t_vec3			vec3_copy(t_vec3 vec1, t_vec3 vec2);

void			mat4_set(t_mat4 *m, float nb);
t_mat4  		mat4_copy(t_mat4 dest, t_mat4 src);

t_mat4			mat4_mul(t_mat4 matrice1, t_mat4 matrice2);
t_mat4  		mat4_sub(t_mat4 matrice1, t_mat4 matrice2);
t_mat4  		mat4_add(t_mat4 matrice1, t_mat4 matrice2);

t_mat4			mat4_rotate_axis(t_mat4 mat, int axis, float angle);
t_mat4  		mat4_transpose(t_mat4 mat);

#endif