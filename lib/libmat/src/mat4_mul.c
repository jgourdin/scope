/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat4_mul.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 15:51:06 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/09 19:03:42 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmat.h"

t_mat4	mat4_mul(t_mat4 matrice1, t_mat4 matrice2)
{
    int i;
    int j;
    int k;
    t_mat4  tmp;

    i = -1;
    while (++i < 4)
    {
        k = -1;
        while(++k < 4)
        {
            j = -1;
            tmp.m[i * 4 + k] = 0;
            while(++j < 4)
                tmp.m[i * 4 + k] += matrice2.m[j * 4 + k] * matrice1.m[i * 4 + j];
        }
    }
    return tmp;
}