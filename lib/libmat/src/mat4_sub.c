/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat4_sub.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 15:53:41 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/09 17:35:34 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmat.h"

t_mat4  mat4_sub(t_mat4 matrice1, t_mat4 matrice2)
{
    int i;

    i = 0;
    while (i < 16)
    {
        matrice1.m[i] -= matrice2.m[i];
        i++;
    }
    return (matrice1);
}