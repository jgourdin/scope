/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat4_transpose.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 16:04:43 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/09 17:43:14 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmat.h"

t_mat4          mat4_transpose(t_mat4 mat)
{
    int         i;
    int         k;
    t_mat4      mat_trans;

    i = -1;
    while (++i < 4)
    {
        k = -1;
        while(++k < 4)
            mat_trans.m[k * 4 + i] = mat.m[i * 4 + k];
    }
    mat = mat4_copy(mat, mat_trans);
    return (mat_trans);
}