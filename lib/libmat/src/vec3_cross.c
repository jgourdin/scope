/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_cross.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 17:23:05 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/09 18:35:15 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmat.h"

/*
** https://www.mathsisfun.com/algebra/vectors-cross-product.html
*/

t_vec3      vec3_cross(t_vec3 vec1, t_vec3 vec2)
{
    t_vec3  vec3;

    vec3.v[0] =  vec1.v[1] * vec2.v[2] - vec1.v[2] * vec2.v[1];
    vec3.v[1] =  vec1.v[2] * vec2.v[0] - vec1.v[0] * vec2.v[2];
    vec3.v[2] =  vec1.v[0] * vec2.v[1] - vec1.v[1] * vec2.v[0];
    return (vec3);
}