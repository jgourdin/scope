/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat4_set.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 15:27:12 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/09 17:55:39 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmat.h"

void	mat4_set(t_mat4 *matrice, float nb)
{
	int		i;

	i = 0;
	while (i < 16)
	{
		if (nb == IDENTITY)
			matrice->m[i] = i % 5 == 0 ? 1 : 0;
		else
			matrice->m[i] = nb;
		i++;
	}
}