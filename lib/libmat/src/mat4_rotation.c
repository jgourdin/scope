/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat4_rotation.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 15:19:43 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/09 18:35:24 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmat.h"

void	rotation_x(t_mat4 *tmp, float radian)
{
	tmp->m[5] = cos(radian);
	tmp->m[6] = sin(radian);
	tmp->m[9] = -sin(radian);
	tmp->m[10] = cos(radian);
}

void	rotation_y(t_mat4 *tmp, float radian)
{
	tmp->m[0] = cos(radian);
	tmp->m[2] = -sin(radian);
	tmp->m[8] = sin(radian);
	tmp->m[10] = cos(radian);
}

void    rotation_z(t_mat4 *tmp, float radian)
{
	tmp->m[0] = cos(radian);
	tmp->m[1] = sin(radian);
	tmp->m[4] = -sin(radian);
	tmp->m[5] = cos(radian);
}

t_mat4	mat4_rotate_axis(t_mat4 mat, int axis, float angle)
{
    float   radian;
    t_mat4	mat_tmp;

    mat4_set(&mat_tmp, IDENTITY);
    radian = angle * (M_PI / 180);
    if (axis == AXIS_X)
    {
        rotation_x(&mat_tmp, radian);
        mat = mat4_mul(mat, mat_tmp);
    }
    if (axis == AXIS_Y)
    {
        rotation_y(&mat_tmp, radian);
        mat = mat4_mul(mat, mat_tmp);
    }
    if (axis == AXIS_Z)
    {
        rotation_z(&mat_tmp, radian);
        mat = mat4_mul(mat, mat_tmp);
    }
    return (mat);
}