/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_add.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 17:11:48 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/09 17:13:50 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmat.h"

/*
** http://www.fundza.com/vectors/add_subtract/index.html
*/

t_vec3      vec3_add(t_vec3 vec1, t_vec3 vec2)
{
    vec1.v[0] += vec2.v[0];
    vec1.v[1] += vec2.v[1];
    vec1.v[2] += vec2.v[2];
    return (vec1);
}