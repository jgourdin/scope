/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_normalize.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 14:38:39 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/09 17:54:21 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmat.h"

/*
** http://www.fundza.com/vectors/normalize/index.html
*/

t_vec3      vec3_normalize(t_vec3 vec3)
{
    float   magnitude;

    magnitude = vec3_magnitude(vec3);
    vec3.v[0] /= magnitude;
    vec3.v[1] /= magnitude;
    vec3.v[2] /= magnitude;
    return (vec3);
}