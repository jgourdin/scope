/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_vec.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 14:35:20 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/09 18:36:24 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libmat.h"

t_vec3	vec3(float x, float y, float z)
{
	t_vec3	vec3;

	vec3.v[0] = x;
	vec3.v[1] = y;
	vec3.v[2] = z;
	return (vec3);
}

t_vec4	vec4(float x, float y, float z, float w)
{
	t_vec4	vec4;

	vec4.v[0] = x;
	vec4.v[1] = y;
	vec4.v[2] = z;
	vec4.v[3] = w;
	return (vec4);
}