/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scope.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: DrunkenWizard <DrunkenWizard@student.42    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 15:02:00 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/14 22:40:34 by DrunkenWiza      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */



#ifndef SCOPE_H
# define SCOPE_H

#define IDENTITY 0x7FFFFFFF
#define SCR_WIDTH 800
#define SCR_HEIGHT 600
#define TRUE 1

# include <errno.h>
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <math.h>
# include <time.h>
# include "lib/libft/includes/libft.h"
# include "libmat.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
# include <stdio.h>
# include <stdint.h>
# include <pthread.h>
# include <OpenGL/gl3.h>
# include <GLFW/glfw3.h>

typedef struct          s_texture
{
    unsigned char       *img;
    int                 width;
    int                 height;
    int				    sizeline;
    int				    size;
    short			    bitperpixel;
    short			    octetperpixel;
    GLuint              texture;
}                       t_texture;

typedef struct          s_shader
{
    GLuint              shaderProgram;
    GLint               flag;
}                       t_shader;

typedef struct          s_matrice
{
    t_mat4              transform;
    t_mat4              rotation;
    t_mat4              translation;
    t_mat4              view;
    t_mat4              projection;
    t_mat4              model;
}                       t_matrice;

typedef struct          s_program
{
    GLuint              model;
    GLuint              magnitude;
}                       t_program;


typedef struct  s_camera
{
    t_vec3      cameraPos;
    t_vec3      cameraTarget;
    t_vec3      cameraDirection;
    t_vec3      up;
    t_vec3      cameraRight;
    t_vec3      cameraUp;
}               t_camera;

typedef struct  s_model
{
    GLfloat     *vertex;
    GLuint      *indice;
    size_t      depth;
    size_t      svertex;
    size_t      sindice;
    size_t      nindice;
    GLuint      VAO;
}               t_model;


typedef struct      s_env
{
    GLFWwindow*     window;
    t_matrice       matrice;
    t_shader        shader;
    t_camera        cam;
    t_program       prog;
    t_model         *model;
    t_texture       *texture;
    int             mindex;
    int             tindex;
    int             nb_model;
    int             texture_flag;
    int             nb_text;
    int             once_boy;
}                   t_env;


int         shader_building(t_env *env);
int         buffer_building(t_model *model);
void        create_texture(t_texture *texture);
void	    load_bmp(t_texture *texture, char *filename);
t_mat4	    rotate(t_mat4 m, t_vec3 v);
t_mat4      translate(t_mat4 m, t_vec3 v);
int         get_next_obj(t_model *model, char *filename);
void	    load_obj(t_env *e, char *filename);

void        init_camera(t_env *env);
void        lookAt(t_env *env);
int         keyPressed(t_env *env, int keycode);
void        openGlEvent(t_env *env);
void        updateShader(t_env *env);
void        processInput(GLFWwindow *window);

int         init_window(t_env *env);
int         init(t_env *env);
void	    set_projection_matrix(t_env *env, float fov);
void        mat_set(t_env *env);
void        framebuffer_size_callback(GLFWwindow* window, int width, int height);
int         print_model(t_model model);

void        set_scene_info(t_model *model, size_t lin, size_t lver);

char        *read_directory(t_env *env, char *path, char *term, int flag);

#endif
