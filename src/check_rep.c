/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_rep.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/14 16:42:02 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/14 18:15:44 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scope.h"

int		check_file(char *filename, char *term)
{
	char	**check;
	int		i;

    i = -1;
	while(filename[i] != '\0')
        i++;
    if (i > 4 && filename[i - 1] == term[3] && filename[i - 2] == term[2] &&
        filename[i - 3] == term[1] && filename[i - 4] == term[0])
    {
        return (1);
    }
	return (0);
}

int     get_nbr_file(char *path, char *term)
{
    DIR 			*pDir;
	struct dirent	*pDirent;
    int             i;

    i = 0;
    if (!(pDir = opendir(path)))
		return (0); 
	while ((pDirent = readdir(pDir)) != NULL)
    {
        if (check_file(pDirent->d_name, term))
            i++;
    }
    return (i);
}

void   detroit_of_your_dream(t_env *env, int index, char *path, int flag)
{
    if (flag == 0)
    {
        get_next_obj(&env->model[index], path);
        buffer_building(&env->model[index]);
    }
    else
    {
        load_bmp(&env->texture[index], path);
        create_texture(&env->texture[index]);
    }
}

void    alloc_or_nut(int flag, t_env *env, int length)
{
    if (flag == 0)
    {
        env->nb_model = length;
        env->model = (t_model *)malloc(sizeof(t_model) * length);
    }
    else
    {
        env->nb_text = length;
        env->texture = (t_texture *)malloc(sizeof(t_texture) * length);
    }
}

char   *read_directory(t_env *env, char *path, char *term, int flag)
{
    DIR 			*pDir;
	struct dirent	*pDirent;
    int             i;

    i = 0;
    alloc_or_nut(flag, env, get_nbr_file(path, term));
	if (!(pDir = opendir(path)))
		return (NULL);
	while ((pDirent = readdir(pDir)) != NULL)
    {
        if(check_file(pDirent->d_name, term))
        {
            detroit_of_your_dream(env, i, ft_strjoin(path, pDirent->d_name), flag);
            i++;
        }
    }
	free(pDirent);
	closedir(pDir);
	return (NULL);
}