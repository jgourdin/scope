#include "../scope.h"

void    change_color(float *red, float *blue, float *green)
{
    if (*red < 1.0f)
        *red += 0.01f;
    else
        *red = 0.0f;
    if (*green < 1.0f)
        *green += 0.0001f;
    else
        *green = 0.0f;
    if (*blue < 1.0f)
        *blue += 0.001f;
    else
        *blue = 0.0f;
}

int main(int argc, char **argv)
{
    t_env   env;
    float   red = 1.0f;
    float   green = 0.5f;
    float   blue = 0.0f;

    if (init(&env))
    {
        printf("Init in the error");
        return(0);
    }
    init_camera(&env);
    mat_set(&env);

    while (!glfwWindowShouldClose(env.window))
    {
        glfwPollEvents();
        processInput(env.window);
        change_color(&red, &blue, &green);
        glClearColor(red, green, blue, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        openGlEvent(&env);
        glUseProgram(env.shader.shaderProgram);
        updateShader(&env);
        glBindTexture(GL_TEXTURE_2D, env.texture[env.tindex].texture);
        glBindVertexArray(env.model[env.mindex].VAO);
        glDrawElements(GL_TRIANGLES, env.model[env.mindex].nindice, \
			GL_UNSIGNED_INT, 0);
        glfwSwapBuffers(env.window);
    }

    glDeleteVertexArrays(1, &(env.model[env.mindex].VAO));
    glfwTerminate();
    return 0;
}

void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, TRUE);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}
