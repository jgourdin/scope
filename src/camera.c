/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 14:14:43 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/11 16:47:32 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scope.h"

void    init_camera(t_env *env)
{
    env->cam.cameraPos = vec3(0.0f, 0.0f, env->model[env->mindex].depth * 2);
    env->cam.cameraTarget = vec3(0.0f, 0.0f, 0.0f);
    env->cam.cameraDirection = vec3_normalize(vec3_sub(env->cam.cameraPos, env->cam.cameraTarget));
    env->cam.cameraRight = vec3_normalize(vec3_cross(vec3(0, 1, 0), env->cam.cameraDirection));
    env->cam.up = vec3_cross(env->cam.cameraDirection, env->cam.cameraRight); 
    env->cam.cameraUp = vec3_cross(env->cam.up, env->cam.cameraRight);
}

/*
** https://stackoverflow.com/questions/19740463/lookat-function-im-going-crazy
** Freaking hard to understand T.T
*/

void lookAt(t_env *env)
{ 
    t_vec3  tmp;
    t_mat4 camToWorld;

    tmp = env->cam.up;
    env->cam.cameraUp = vec3_normalize(vec3_sub(env->cam.cameraPos, env->cam.cameraTarget)); 
    env->cam.cameraRight =  vec3_normalize(vec3_cross(env->cam.up, env->cam.cameraUp)); 
    tmp = vec3_cross(env->cam.cameraUp, env->cam.cameraRight); 

    mat4_set(&camToWorld, IDENTITY);
    camToWorld.m[0] = env->cam.cameraRight.v[0];
	camToWorld.m[1] = tmp.v[0];
	camToWorld.m[2] = env->cam.cameraUp.v[0];
	camToWorld.m[4] = env->cam.cameraRight.v[1];
	camToWorld.m[5] = tmp.v[1];
	camToWorld.m[6] = env->cam.cameraUp.v[1];
	camToWorld.m[8] = env->cam.cameraRight.v[2];
	camToWorld.m[9] = tmp.v[2];
	camToWorld.m[10] = env->cam.cameraUp.v[2];
	camToWorld.m[12] = -vec3_dot(env->cam.cameraRight, env->cam.cameraPos);
	camToWorld.m[13] = -vec3_dot(tmp, env->cam.cameraPos);
	camToWorld.m[14] = -vec3_dot(env->cam.cameraUp, env->cam.cameraPos);
    env->matrice.view = camToWorld;
} 