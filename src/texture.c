/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 13:43:08 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/14 18:27:17 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scope.h"

/*
**  FILE    fopen(nom-de-fichier , mode) => "r" = lecture
**
**  int     fseek(FILE *stream, long int offset, int whence)
**  stream => pointer to file
**  offset => number of bytes from whence
**  whence => the position where the offset is added
**  SEEK_SET = Beginning of file
**  SEEK_CUR = Current position of the file pointer
**  SEEK_END = End of file
**
**  size_t  fread(void *ptr, size_t size, size_t nmemb, FILE *stream);
**  lit NMEMB éléments de données, chacun d'eux représentant
**  SIZE octets de long, depuis le flux pointé par STREAM,
**  et les stocke à l'emplacement pointé par PTR.
*/

void	get_image(t_texture *texture, char *buffer, int i)
{
	int	h;
	int	j;
	int	size;

	h = 0;
	size = texture->size * 2;
	texture->img = (unsigned char*)malloc(sizeof(unsigned char) * size);
	while (i >= 0)
	{
		i -= texture->sizeline;
		j = 0;
		while (j < texture->sizeline)
		{
			texture->img[h + j] = (unsigned char)buffer[i + j + 2];
			texture->img[h + j + 1] = (unsigned char)buffer[i + j + 1];
			texture->img[h + j + 2] = (unsigned char)buffer[i + j];
			j += 3;
		}
		h += texture->sizeline;
	}
}

void	read_header(char *filename, t_texture *texture)
{
	FILE	*file;

	if ((file = fopen(filename, "r")) == NULL)
		exit(-1);
	fseek(file, 18, SEEK_SET);
	fread(&texture->width, 4, 1, file);
	fread(&texture->height, 4, 1, file);
	fseek(file, 2, SEEK_CUR);
	fread(&texture->bitperpixel, 2, 1, file);
	fclose(file);
	texture->octetperpixel = texture->bitperpixel / 8;
	texture->sizeline = texture->width * texture->octetperpixel;
	texture->width < 0 ? texture->width = -texture->width : 0;
	texture->height < 0 ? texture->height = -texture->height : 0;
	texture->size = texture->sizeline * texture->height;
}

void	load_bmp(t_texture *texture, char *filename)
{
	int		fd;
	int		i;
	char	*buffer;

	read_header(filename, texture);
	buffer = (char*)malloc(sizeof(char) * texture->size + 1);
	if ((fd = open(filename, O_RDWR)) == -1)
		return;
	lseek(fd, 54, SEEK_SET);
	i = read(fd, buffer, texture->size);
	get_image(texture, buffer, i);
	ft_strdel((char**)&buffer);
	close(fd);
}

void	create_texture(t_texture *texture)
{
	glGenTextures(1, &texture->texture);
	glBindTexture(GL_TEXTURE_2D, texture->texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->width,
	texture->height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture->img);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}