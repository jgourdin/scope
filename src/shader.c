/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: DrunkenWizard <DrunkenWizard@student.42    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/03 15:37:26 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/14 22:53:11 by DrunkenWiza      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scope.h"

const GLchar	*parseShader(char *filename)
{
	int		fd;
	int		i;
	char	buffer[2500];
	char	*shaderString;
	char	*tmp;

	shaderString = ft_strnew(2500);
	if ((fd = open(filename, O_RDONLY)) == -1)
    {
		ft_putendl("Cannot opened shader file");
        return (NULL);
    }
	while ((i = read(fd, buffer, 2500)))
	{
		buffer[i] = '\0';
		tmp = shaderString;
		shaderString = ft_strjoin(shaderString, buffer);
		ft_strdel(&tmp);
	}
	close(fd);
	return(shaderString);
}

GLuint    createShader(char *filename, GLenum shaderType)
{
    int             shader;
    int             success;
    const GLchar    *shaderString;
    char            infoLog[512];
    
    shaderString = parseShader(filename);
    shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &shaderString, NULL);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    { 
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED:: %s\n", infoLog);
        return(-1);
    }
    return(shader);
}

int    link_shader(GLuint vertex_shader, GLuint fragment_shader)
{
    int shaderProgram;
    int success;
    char infoLog[512];

    if (vertex_shader == -1 || fragment_shader == -1)
        return (0);
    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertex_shader);
    glAttachShader(shaderProgram, fragment_shader);
    glLinkProgram(shaderProgram);

    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        printf("ERROR::SHADER::PROGRAM::LINKING_FAILED %s\n", infoLog);
    }
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    
    return (shaderProgram);
}

int     buffer_building(t_model *model)
{
    GLuint      VBO;
    GLuint      EBO;
    glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
    glGenVertexArrays(1, &model->VAO);
    glBindVertexArray(model->VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, model->svertex,
		model->vertex, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, model->sindice,
		model->indice, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
		(GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
		(GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
    return (1);
}

int     shader_building(t_env *env)
{   
    if (!(env->shader.shaderProgram = link_shader(createShader("shader/vertex", GL_VERTEX_SHADER), createShader("shader/fragment", GL_FRAGMENT_SHADER))))
        return (-1);
    //buffer_building(&env->model[env->mindex]);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // For The Wireframe mode
    ft_putstr("Shader building Complete\n");
    return (1);
}
