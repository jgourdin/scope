/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 14:18:13 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/14 19:00:55 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scope.h"

int         keyPressed(t_env *env, int keycode)
{
    if (glfwGetKey(env->window, keycode) == GLFW_PRESS)
        return (1);
    return (0);
}

int         keyUnpressed(t_env *env, int keycode)
{
    if (glfwGetKey(env->window, keycode) == GLFW_RELEASE)
        return (1);
    return (0);
}

void		change_obj(t_env *env, int flag)
{
	if (flag == 0 && env->mindex > 0)
		env->mindex--;
	else if (flag == 0 && env->mindex == 0)
		env->mindex = env->nb_model - 1;
	else if (flag == 1 && env->mindex < env->nb_model - 1)
		env->mindex++;
	else if (flag == 1 && env->mindex == env->nb_model - 1)
		env->mindex = 0;
	env->once_boy = 1;
	init_camera(env);
}

void		change_tex(t_env *env, int flag)
{
	if (flag == 0 && env->tindex > 0)
		env->tindex--;
	else if (flag == 0 && env->tindex == 0)
		env->tindex = env->nb_text - 1;
	else if (flag == 1 && env->tindex < env->nb_text - 1)
		env->tindex++;
	else if (flag == 1 && env->tindex == env->nb_text - 1)
		env->tindex = 0;
	env->once_boy = 1;
}

void        openGlEvent(t_env *env)
{
    if (keyPressed(env, GLFW_KEY_6))
		env->matrice.translation = translate(env->matrice.translation, vec3(0, 0, 0.2));
	if (keyPressed(env, GLFW_KEY_5))
		env->matrice.translation = translate(env->matrice.translation, vec3(0, 0, -0.2));
	if (keyPressed(env, GLFW_KEY_4))
		env->matrice.translation = translate(env->matrice.translation, vec3(0, 0.01, 0));
	if (keyPressed(env, GLFW_KEY_3))
		env->matrice.translation = translate(env->matrice.translation, vec3(0, -0.01, 0));
	if (keyPressed(env, GLFW_KEY_2))
		env->matrice.translation = translate(env->matrice.translation, vec3(0.01, 0, 0));
	if (keyPressed(env, GLFW_KEY_1))
		env->matrice.translation = translate(env->matrice.translation, vec3(-0.01, 0, 0));
	if (keyPressed(env, GLFW_KEY_W))
		env->matrice.rotation = rotate(env->matrice.rotation, vec3(0.5, 0, 0));
	if (keyPressed(env, GLFW_KEY_S))
		env->matrice.rotation = rotate(env->matrice.rotation, vec3(-0.5, 0, 0));
	if (keyPressed(env, GLFW_KEY_A))
		env->matrice.rotation = rotate(env->matrice.rotation, vec3(0, 0, 0.5));
	if (keyPressed(env, GLFW_KEY_D))
		env->matrice.rotation = rotate(env->matrice.rotation, vec3(0, 0, -0.5));
	if (keyPressed(env, GLFW_KEY_SPACE) && env->once_boy == 0)
	{
		env->texture_flag = env->texture_flag == 0 ? 1 : 0;
		env->once_boy = 1;
	}
	if (keyPressed(env, GLFW_KEY_LEFT) && env->once_boy == 0)
		change_obj(env, 0);
	if (keyPressed(env, GLFW_KEY_RIGHT) && env->once_boy == 0)
		change_obj(env, 1);
	if (keyPressed(env, GLFW_KEY_KP_ADD) && env->once_boy == 0)
		change_tex(env, 0);
	if (keyPressed(env, GLFW_KEY_KP_SUBTRACT) && env->once_boy == 0)
		change_tex(env, 1);
	if (keyUnpressed(env, GLFW_KEY_LEFT) && keyUnpressed(env, GLFW_KEY_RIGHT) &&
			keyUnpressed(env, GLFW_KEY_KP_ADD) && keyUnpressed(env, GLFW_KEY_KP_SUBTRACT) && 
			keyUnpressed(env, GLFW_KEY_SPACE))
	{
		env->once_boy = 0;
	}
    lookAt(env);
    glUniform1f(env->prog.magnitude, env->model[env->mindex].depth);
    //env->matrice.translation = translate(env->matrice.translation, vec3(0, 0, 0.2));
}

void        updateShader(t_env *env)
{
	env->matrice.rotation = rotate(env->matrice.rotation, vec3(0, 0.5, 0));
    env->matrice.model = mat4_mul(env->matrice.translation, env->matrice.rotation);
    env->matrice.transform = mat4_mul(mat4_transpose(env->matrice.model), 
        mat4_mul(env->matrice.view, env->matrice.projection));
    glUniformMatrix4fv(env->prog.model, 1, GL_FALSE, env->matrice.transform.m);
    glUniform1f(env->prog.magnitude, env->model[env->mindex].depth);
	glUniform1i(env->shader.flag, env->texture_flag);
}