/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transform.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: DrunkenWizard <DrunkenWizard@student.42    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/19 14:21:39 by DrunkenWiza       #+#    #+#             */
/*   Updated: 2019/03/19 19:07:52 by DrunkenWiza      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scope.h"

t_mat4    translate(t_mat4 m, t_vec3 v)
{
    m.m[3] += v.v[0];
	m.m[7] += v.v[1];
	m.m[11] += v.v[2];
    return (m);
}

t_mat4	rotate(t_mat4 m, t_vec3 v)
{
	if (v.v[0] != 0.0)
		m = mat4_rotate_axis(m, 0, v.v[0]);
	if (v.v[1] != 0.0)
		m = mat4_rotate_axis(m, 1, v.v[1]);
	if (v.v[2] != 0.0)
		m = mat4_rotate_axis(m, 2, v.v[2]);
    return (m);
}

t_mat4  init_mat4(float i)
{
    t_mat4  mat;

    mat.m[0] = i;
	mat.m[1] = 0;
	mat.m[2] = 0;
	mat.m[3] = 0;
	mat.m[4] = 0;
	mat.m[5] = i;
	mat.m[6] = 0;
	mat.m[7] = 0;
	mat.m[8] = 0;
	mat.m[9] = 0;
	mat.m[10] = i;
	mat.m[11] = 0;
	mat.m[12] = 0;
	mat.m[13] = 0;
	mat.m[14] = 0;
	mat.m[15] = i;
	return (mat);
}