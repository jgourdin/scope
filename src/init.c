/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 14:17:31 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/14 18:47:50 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scope.h"

int    init_window(t_env *env)
{
    env->window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "SCOPE_JOJO", NULL, NULL);
    if (env->window == NULL)
    {
        printf("Failed to create GLFW window");
        glfwTerminate();
        return(1);
    }
    glfwMakeContextCurrent(env->window);
    glfwSetFramebufferSizeCallback(env->window, framebuffer_size_callback);
    glfwSetInputMode(env->window, GLFW_STICKY_KEYS, 1);
    return(0);
}

int     init(t_env *env)
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    if(init_window(env))
        return (1);
    env->mindex = 0;
    env->tindex = 0;
    env->once_boy = 0;
    env->texture_flag = 0;
    glEnable(GL_DEPTH_TEST);
    shader_building(env);
    read_directory(env, "objets/", ".obj", 0);
    read_directory(env, "texture/", ".bmp", 1);
    mat4_set(&env->matrice.transform, IDENTITY);
    return (0);
}

/*
** I'm lost haha but go see for help
** https://www.scratchapixel.com/lessons/3d-basic-rendering/perspective-and-orthographic-projection-matrix/building-basic-perspective-projection-matrix
*/

void	set_projection_matrix(t_env *env, float fov)
{
	float	scale;
	float	far;
	float	near;

	far = 10000;
	near = 0.001;
	scale = 1 / (tan(fov * 0.5 * M_PI / 180.0));
	mat4_set(&env->matrice.projection, IDENTITY);
	env->matrice.projection.m[0] = scale / ((float)SCR_WIDTH / (float)SCR_HEIGHT);
	env->matrice.projection.m[5] = scale;
	env->matrice.projection.m[10] = -(far + near) / (far - near);
	env->matrice.projection.m[11] = -1;
	env->matrice.projection.m[14] = -2 * far * near / (far - near);
}

void    mat_set(t_env *env)
{
    mat4_set(&env->matrice.model, IDENTITY);
	mat4_set(&env->matrice.view, IDENTITY);
	mat4_set(&env->matrice.rotation, IDENTITY);
	mat4_set(&env->matrice.translation, IDENTITY);
    mat4_set(&env->matrice.transform, IDENTITY);
    mat4_set(&env->matrice.projection, IDENTITY);
    set_projection_matrix(env, 90);  
    env->matrice.transform = rotate(env->matrice.transform, vec3( 0.0f, 0.5f, 0.0f));
    env->matrice.model = mat4_mul(env->matrice.translation, env->matrice.rotation);
    env->prog.model = glGetUniformLocation(env->shader.shaderProgram, "matrice");
    env->prog.magnitude = glGetUniformLocation(env->shader.shaderProgram, "magnitude");
    env->shader.flag = glGetUniformLocation(env->shader.shaderProgram, "flag");
}