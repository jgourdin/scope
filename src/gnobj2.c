/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnobj2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/14 16:12:46 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/14 16:21:36 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scope.h"


void	center_all_vertices(GLfloat *vertex, int length, t_vec3 center)
{
	int		i;

	i = 0;
	while (i < length)
	{
		vertex[i] -= center.v[0];
		vertex[i + 1] -= center.v[1];
		vertex[i + 2] -= center.v[2];
		i += 6;
	}
}

void	center_search(GLfloat *vertices, int num_vertices)
{
	int		i;
	t_vec3	max;
	t_vec3	min;
	t_vec3	center;

	i = 0;
	max = vec3(0, 0, 0);
	min = vec3(0, 0, 0);
	while (i < num_vertices - 6)
	{
		vertices[i] > max.v[0] ? max.v[0] = vertices[i] : 0;
		vertices[i] < min.v[0] ? min.v[0] = vertices[i] : 0;
		vertices[i + 1] > max.v[1] ? max.v[1] = vertices[i + 1] : 0;
		vertices[i + 1] < min.v[1] ? min.v[1] = vertices[i + 1] : 0;
		vertices[i + 2] > max.v[2] ? max.v[2] = vertices[i + 2] : 0;
		vertices[i + 2] < min.v[2] ? min.v[2] = vertices[i + 2] : 0;
		i += 6;
	}
	center = vec3_fmul(vec3_add(max, min), 0.5);
	center_all_vertices(vertices, num_vertices, center);
}

void        set_scene_info(t_model *model, size_t lin, size_t lver)
{
    int     i;

    i = 0;
    center_search(model->vertex, lver);
    model->depth = 0;
    while (model->vertex[i])
	{
		if (model->depth < model->vertex[i + 2])
			model->depth = model->vertex[i + 2];
		i += 6;
	}
    model->svertex = lver * sizeof(GLfloat) * 2;
	model->sindice = lin * sizeof(GLuint);
	model->nindice = lin;
}