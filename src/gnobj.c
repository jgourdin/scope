/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnobj.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 21:34:43 by DrunkenWiza       #+#    #+#             */
/*   Updated: 2019/04/14 16:31:05 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scope.h"

size_t          ft_dstrlen(char **str)
{
    int	i;

	i = 0;
	while (str[i] != NULL)
        i++;
	return (i);
}

t_list          *add_to_lst(char *tmp, t_list *vertex)
{
    t_list  *new;

    tmp = tmp + 2;
    new = malloc(sizeof(t_list));
    new->content = ft_strsplit(tmp, ' ');
    new->content_size = ft_strlen(tmp);
    new->next = vertex;
    return(new);
}

void        create_ver(t_model *model, t_list *lst, size_t length)
{
    char    **tmp;
    
    model->vertex = malloc(sizeof(GLfloat) * length);
    while(lst != NULL)
    {
        tmp = lst->content;
        model->vertex[--length] = (GLfloat)atof(tmp[2]);
        model->vertex[--length] = (GLfloat)atof(tmp[1]);
        model->vertex[--length] = (GLfloat)atof(tmp[0]);
        lst = lst->next;
    }
}

void        create_ind(t_model *model, t_list *lst, size_t length)
{
    char    **tmp;

    model->indice = malloc(sizeof(GLint) * length);
    while(lst != NULL)
    {
        tmp = lst->content;
        if (ft_dstrlen(tmp) == 4)
        {
            model->indice[--length] = (GLint)(atoi(tmp[3]) - 1);
            model->indice[--length] = (GLint)(atoi(tmp[2]) - 1);
            model->indice[--length] = (GLint)(atoi(tmp[0]) - 1);
        }
        model->indice[--length] = (GLint)(atoi(tmp[2]) - 1);
        model->indice[--length] = (GLint)(atoi(tmp[1]) - 1);
        model->indice[--length] = (GLint)(atoi(tmp[0]) - 1);
        lst = lst->next;
    }
}

int         get_next_obj(t_model *model, char *filename)
{
    int         fd;
    char        *tmp;
    int         lver = 0;
    int         lin = 0;
    t_list      *lstVertex = NULL;
    t_list      *lstIndices = NULL;

    if ((fd = open(filename, O_RDWR)) == -1)
        return (-1);
    while(get_next_line(fd, &tmp) > 0)
    {
        if (tmp[0] == 'v' && tmp[1] == ' ')
        {
            lstVertex = add_to_lst(tmp, lstVertex);
            lstVertex = add_to_lst(tmp, lstVertex);
            lver += 6;
        }
        else if (tmp[0] == 'f' && tmp[1] == ' ')
        {
            lstIndices = add_to_lst(tmp, lstIndices);
            lin += (ft_dstrlen(lstIndices->content) == 3 ? 3 : 6);
        }
        tmp = NULL;
    }
    create_ver(model, lstVertex, lver);
    create_ind(model, lstIndices, lin);
    set_scene_info(model, lin, lver);
    close(fd);
    return 1;
}