/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 15:01:50 by jgourdin          #+#    #+#             */
/*   Updated: 2019/01/30 16:17:55 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scope.h"

int         key_hook(int keycode, t_ptr *ptr)
{   
    if (keycode == 12)
        ptr->mode.epileptic = ptr->mode.epileptic == 1 ? 0 : 1;
    return (1);
}

int         hook(t_ptr  *ptr)
{
    processInput(ptr->win_ptr);

    if (ptr->mode.epileptic == 1)
        EpilepticMode(ptr); 
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    //glUseProgram(ptr->shaderProgram);
    glBindVertexArray(ptr->VAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);

    mlx_opengl_swap_buffers(ptr->win_ptr);
    mlx_key_hook(ptr->win_ptr, &key_hook, ptr);
    return (1);
}

void processInput(void *win_ptr)
{
}

void framebuffer_size_callback(void *win_ptr, int width, int height)
{
    glViewport(0, 0, width, height);
}
