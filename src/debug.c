/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgourdin <jgourdin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 14:41:00 by jgourdin          #+#    #+#             */
/*   Updated: 2019/04/11 14:49:42 by jgourdin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../scope.h"

int     print_model(t_model model)
{
    int     i;

    i = 0;
    printf("debug model :\n");
    printf("nindice = %zu || sindice = %zu\n", model.nindice, model.sindice);
    printf("depth = %zu || svertex = %zu\n", model.depth, model.svertex);
    printf("indice:\n");
    while(i < model.nindice)
    {
        printf("%u ", model.indice[i++]);
        printf("%u ", model.indice[i++]);
        printf("%u \n", model.indice[i++]);
    }
    return (1);
}